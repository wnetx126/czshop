<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

            /**函数定义  函数名称以字母或下划线开头（不能以数字开头）
             * function 函数名称(参数1,,,,参数n){
             *    代码块
             * }
             * 
                function writeName()
                {
                    echo "Kai Jim Refsnes";
                }

                echo "My name is ";
                writeName();
             */
            //返回到上个页面
            function goback($msg){
                echo('<script language="javascript">alert("'.$msg.'"); history.back();</script>');
                exit();
            }
            //前进或跳转到某个页面
            function goforward($msg,$url){
                echo('<script language="javascript">alert("'.$msg.'"); location.href="'.$url.'";</script>');
                exit();
            }
          /**
            * 生成随机字符串实例代码（字母+数字）
            * 生成一个随机字符串时，总是先创建一个字符池，然后用一个循环和mt_rand()或rand()生成php随机数，从字符池中随机选取字符，最后拼凑出需要的长度
            * @param type $length
            * @return string
            * 返回值 如果是6个长度返回: MII3J6
            */        
           function randomkeys($length) { 
            $pattern ="1234567890ABCDEFGHIJKLOMNOPQRSTUVWXYZ";
            $key="";
            for($i=0;$i<$length;$i++){ 
             $key .= $pattern{mt_rand(0,36)}; //生成php随机数 
            } 
            return $key; 
           } 
           /**
            * 生成订单号
            * @return string
            * 返回值类似: 20201125081130MII3J6
            */
           function generateNum(){
               $order_number =  date('Ymdhms').randomkeys(6);
               return $order_number;
           }
        ?>
    </body>
</html>
