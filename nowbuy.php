<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
    <?php
        include_once 'islogin.php';
        include_once 'functions.php';
    ?>
    <?php
        /**
        * 功能：详情页-立即购买
        * 描述：在详情页中立即购买并跳转到订单列表页面
        *      订单表（订单编码，用户ID，商品总价）
        *      订单详情表（订单编码，用户ID，商品ID，商品数量）
        * 思路：1.生成订单表数据；
        *       2.生成订单详情表数据
        *       3.跳转到订单列表页
        */
        //一:接收前端传递过来的参数 post提交过来的 表单域组件  
        //表单域组件[主要是input组件 ]
        echo "立即购买<br>";
         var_dump($_POST);
         $gid=$_POST['gid'];//商品ID的数组
         $num=$_POST['num'];//商品数量的数组
         $price=$_POST['price'];//商品单价的数组
         $totalPrice=$num*$price;//商品的总价（所有商品的价格的和）
         $userId=$_COOKIE['czuid'];//用户的ID
         $orderCode=  generateNum();//生成订单号
         echo "<br>$gid $num $price $totalPrice $userId $orderCode<br>";
//         exit();
        //二:把接收的参数插入到数据表中
        //1.连接数据库
        $link=mysqli_connect("localhost", "root", "");//创建数据库的连接
        mysqli_select_db($link, "itcast");//选择要使用数据库
        mysqli_set_charset($link, "utf8");//设置连接的字符编码格式
        //2.操作数据表，写SQL模板
        //2A.把订单数据插入订单表中
        //INSERT INTO shop_order(user_id,order_code,goods,address,price,cancel,payment)VALUES(14,'202011250911101KW8HO','','南华工商学院实验楼316',168,'no','no');
        $sql="INSERT INTO shop_order(user_id,order_code,goods,address,price,cancel,payment)VALUES($userId,'$orderCode','','南华工商学院实验楼316',$totalPrice,'no','no');";
        echo "订单表 $sql<br>";
        $result=  mysqli_query($link, $sql); //mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
//        exit();
        //2B.把订单数据插入订单详情表中
        //INSERT INTO shop_order_detail(user_id,order_id,order_code,good_id,good_num,good_price)VALUES(14,0,'20201125091113NYN9HY',28,2,56.00);
        $sqlDetail="INSERT INTO shop_order_detail(user_id,order_id,order_code,good_id,good_num,good_price)VALUES($userId,0,'$orderCode',$gid,$num,$price);";
        echo "订单详情表 $sqlDetail<br>";
        $result=  mysqli_query($link, $sqlDetail);//mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
//      exit();
        //3.处理结果记录
        if($result){
            echo "<script>alert('立即购买成功！');location.href='order.php'</script>";
        }else{
            echo "<script>alert('立即购买失败！');;location.href='order.php'</script>";
        }
    ?>
    </body>
</html>
