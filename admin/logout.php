<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // 用户登录注销 原理：让用户登录的cookie信息过期
        if(isset($_COOKIE['czaid'])){
            //setcookie(键,值,过期时间);  //过期时间=当前时间+时间间隔=time()+3600*24*7
            setcookie("czaid",null, time());  //3600秒 * 24小时* 7天
            setcookie("czaname",null, time());
        }
        //跳转到后台首页
        header("Location:index.php");
        ?>
    </body>
</html>
