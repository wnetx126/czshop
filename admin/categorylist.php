<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>传智商城 - 后台管理系统</title>
	<link rel="stylesheet" href="../Public/Admin/css/style.css"/>
	<script src="../Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php include_once 'top.php'; ?>
<div class="main">
        <?php include_once 'left.php'; ?>
	<div class="content">
		<div class="item"><div class="title">商品分类</div>
<div class="top-button">
    相关操作：<a href="categoryadd.php" class="light">添加分类</a>
</div>
<div class="list full auto">
	<table>
		<tr><th>分类名称</th><th>操作</th></tr>
                <?php
                //1.连接数据库
                $link=mysqli_connect("localhost", "root", "");//创建数据库的连接
                mysqli_select_db($link, "itcast");//选择要使用数据库
                mysqli_set_charset($link, "utf8");//设置连接的字符编码格式
                //2.操作数据表 写SQL模板
                $sql="SELECT * FROM shop_category;";
                $result=  mysqli_query($link, $sql); //mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
                //处理结果记录
                while ($row = mysqli_fetch_assoc($result)) {
                    $id=$row["id"];
                    $name=$row['name'];
//                    echo "<tr><td>图书</td></tr>";
                ?>
                <tr><td><?php echo $name; ?></td><td><a href="categoryedit.php?id=<?php echo $id;?>">修改</a>　<a href="categorydel.php?id=<?php echo $id;?>" class="act-del" data-id="<?php echo $id;?>" >删除</a></td></tr>
                <?php   
                }
                ?>
        </table>
</div>
<form method="post" id="form">
	<input type="hidden" name="id" id="target_id">
<input type="hidden" name="__hash__" value="cdbedfb66f38caf835ffde7e20697afa_49480b652119b3ceeb0d07813e39bdca" /></form>
<script>
	//删除
	$(".act-del").click(function(){
		if(confirm('确定要删除吗？（该分类下的商品将归于未分类）')){
			$("#target_id").val($(this).attr("data-id"));
			$("#form").attr("action","/Admin/Category/del.html").submit();
		}
	});
</script></div>
	</div>
</div>
<script>
	$("#Category_index").addClass("curr");
</script>
</body>
</html>