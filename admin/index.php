<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>传智商城 - 后台管理系统</title>
	<link rel="stylesheet" href="../Public/Admin/css/style.css"/>
	<script src="../Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php include_once 'top.php'; ?>
<div class="main">
        <?php include_once 'left.php'; ?>
	<div class="content">
		<div class="item"><div class="title">后台首页</div>
<dl class="bordered">
	<dt>欢迎访问</dt>
	<dd>欢迎进入传智商城后台！请从左侧选择一个操作。</dd>
	<dd></dd>
</dl>
<dl class="bordered">
	<dt>服务器信息</dt>
	<dd>系统环境：Apache/2.2.29 (Win32) PHP/5.4.38</dd>
	<dd>服务器时间：2015-05-21 18:36:41</dd>
	<dd>MySQL版本：5.5.43</dd>
	<dd>文件上传限制：2M</dd>
	<dd>脚本执行时限：30秒</dd>
</dl></div>
	</div>
</div>
<script>
	$("#Index_index").addClass("curr");
</script>
</body>
</html>