<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>传智商城 - 后台管理系统</title>
	<link rel="stylesheet" href="./Public/Admin/css/style.css"/>
	<script src="./Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php include_once 'top.php'; ?>
<div class="main">
        <?php include_once 'left.php'; ?>
	<div class="content">
		<div class="item"><div class="title">商品回收站</div>
<div class="top-button">
	相关操作：<a href="/Admin/Goods/index.html" class="light">商品列表</a>
</div>
<div class="list full">
	<table>
		<tr><th class="t1">商品分类</th><th>商品名称</th><th width="100">库存</th><th width="60">上架</th><th width="60">推荐</th><th width="120">操作</th></tr>
		<tr><td class="t1">
				IT类			</td>
			<td>测试</td><td>11</td>
			<td>否</td>
			<td>否</td><td>
			<a href="#" class="act-rec" data-id="52">恢复</a>　<a href="#" class="act-del" data-id="52">删除</a></td></tr><tr><td class="t1">
				网页书籍			</td>
			<td>HTML+CSS+JavaScript网页制作案例教程</td><td>96</td>
			<td>是</td>
			<td>否</td><td>
			<a href="#" class="act-rec" data-id="51">恢复</a>　<a href="#" class="act-del" data-id="51">删除</a></td></tr>	</table>
</div>
<div class="pagelist"><div>    </div></div>
<form method="post" id="form">
	<input type="hidden" name="id" id="target_id">
</form>
<script>
	//彻底删除
	$(".act-del").click(function(){
		if(confirm('确定要彻底删除吗？')){
			$("#target_id").val($(this).attr("data-id"));
			$("#form").attr("action","/Admin/Recycle/del/p/0.html").submit();
		}
	});
	//恢复
	$(".act-rec").click(function(){
		$("#target_id").val($(this).attr("data-id"));
		$("#form").attr("action","/Admin/Recycle/rec/p/0.html").submit();
	});
</script></div>
	</div>
</div>
<script>
	$("#Recycle_index").addClass("curr");
</script>
</body>
</html>