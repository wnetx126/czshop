<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>传智商城 - 后台管理系统</title>
	<link rel="stylesheet" href="../Public/Admin/css/style.css"/>
	<script src="../Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php include_once 'top.php'; ?>
<div class="main">
        <?php include_once 'left.php'; ?>
	<div class="content">
		<div class="item"><div class="title">分类修改</div>
<div class="top-button">
    相关操作：<a href="categorylist.php" class="light">分类列表</a>
</div>
<?php
  //一:接收前端浏览器地址栏【超链接】传递过来的参数，通过get获取参数
        $id=$_GET['id'];
        //二:把接收的参数插入到数据表中
        //1.连接数据库
        $link=mysqli_connect("localhost", "root", "");//创建数据库的连接
        mysqli_select_db($link, "itcast");//选择要使用数据库
        mysqli_set_charset($link, "utf8");//设置连接的字符编码格式
        //2.操作数据表，写SQL模板
        $sql="SELECT * FROM shop_category WHERE id=$id;";
        $result=  mysqli_query($link, $sql); //mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
        //3.处理结果记录
        $row=  mysqli_fetch_assoc($result);
        $name=$row['name'];
?>
<div class="list auto">
    <form action="categorymodify.php" method="post">
	<input type="hidden" name="id" value="<?php echo $id; ?>">
	<table class="t2 t3">
		<tr><th>分类名称：</th><td><input type="text" name="name" value="<?php echo $name;?>"></td></tr>
	</table>
	<div class="btn">
		<input type="submit" value="修改分类">
		<input type="reset" value="重新填写">
	</div>
    </form>
</div></div>
	</div>
</div>
<script>
	$("#Category_edit").addClass("curr");
</script>
</body>
</html>