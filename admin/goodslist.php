<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>传智商城 - 后台管理系统</title>
	<link rel="stylesheet" href="../Public/Admin/css/style.css"/>
	<script src="../Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php include_once 'top.php'; ?>
<div class="main">
        <?php include_once 'left.php'; ?>
	<div class="content">
		<div class="item"><div class="title">商品列表</div>
 <?php
//     var_dump($_GET);
    //三元运算  变量= 表达式?值1:值2  例如：  $a= 3>4? 1:0 ;
    //$cid定义分类
    $cid=  isset($_GET['cid'])?$_GET['cid']:0;  
    $cid=  intval($cid);  //intvar(字符串数字)转成数字
 ?>
 <?php
        //1.连接数据库
        $link=mysqli_connect("localhost", "root", "");//创建数据库的连接
        mysqli_select_db($link, "itcast");//选择要使用数据库
        mysqli_set_charset($link, "utf8");//设置连接的字符编码格式
        //2.操作数据库，写SQL模板
 ?>
            
     
<div class="top-button">
	选择商品分类：
            <select id="category">
		<option value="0" <?php if($cid==0) echo "selected"; ?> >全部</option>
                <?php
                 //查询分类列表数据
                $sql2="SELECT * FROM shop_category;";
                $result2=  mysqli_query($link, $sql2);
                echo "<br>$sql2</br>";
                while($row=  mysqli_fetch_assoc($result2)){
                    $ccid=$row['id'];
                    $cname=$row['name'];
//                    echo "<option value='14' >图书</option>";
                ?>
                    <option value='<?php echo $ccid;?>' <?php if($cid==$ccid){echo "selected";} ?> ><?php echo $cname;?></option>
                <?php
                    
                }
               ?> 
            </select>
        <a href="goodsadd.php" class="light">添加商品</a>
        <a href="categoryadd.php">添加分类</a>
</div>
<div class="list full">
	<table>
		<tr><th class="t1">商品分类</th><th>商品名称</th><th width="100">库存</th><th width="60">上架</th><th width="60">推荐</th><th width="120">操作</th></tr>
                <?php
                /**
                SELECT * FROM 表名称 LIMIT 起始值,长度;   起始值从0开始
                SELECT * FROM shop_goods LIMIT 0,6; 0  6*0= 6*(1-1)
                SELECT * FROM shop_goods LIMIT 6,6; 6  6*1= 6*(2-1)
                SELECT * FROM shop_goods LIMIT 12,6; 12 6*2= 6*(3-1)
                SELECT * FROM shop_goods LIMIT 18,6; 18 6*3= 6*(4-1)
                SELECT COUNT(id) AS num FROM shop_goods;
                 */
                //查询商品总记录数
                $sqlNum="SELECT COUNT(id) AS num FROM shop_goods;";
                $resultNum=mysqli_query($link, $sqlNum);
                $rowNum=  mysqli_fetch_assoc($resultNum);
                $count=$rowNum['num'];//总记录数
                //page是通过超链接传递参数
                $page=  isset($_GET['page'])?$_GET['page']:1;
                $page=  intval($page);//页码数
                $psize=6;    //每一页商品数量
                $start=$psize*($page-1);    //每一页起始值
                $maxPage= ceil($count/$psize);  //尾页数即最大页数
                $pre=$page>1?$page-1:1;//上一页
                $next=$page<$maxPage?$page+1:$maxPage;//下一页
                //ceil(值) 取整到最接近的最大的整数 ceil(5.1)  结果是6  ceil(5.4)  结果是6  ceil(5.7)  结果是6 
                //查询商品列表数据
                //SELECT * FROM shop_goods LIMIT 0,6; 分页
//                $sql1="SELECT * FROM shop_goods WHERE category_id=$cid LIMIT $start,$psize;"; 
                $sql1="SELECT * FROM shop_goods  LIMIT $start,$psize;";
                if($cid>0){
                    $sql1="SELECT * FROM shop_goods WHERE category_id=$cid LIMIT $start,$psize;"; 
                }
                echo "<br>$sql1</br>";
                $result1=  mysqli_query($link, $sql1); //mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
                //3.处理查询结果
                while ($row= mysqli_fetch_assoc($result1)) {
                    $categoryId=$row['category_id'];
                    $name=$row['name'];     //商品名称
                    $stock=$row['stock'];   //库存
                    $onSale=$row['on_sale'];//是否上架
                    $recommend=$row['recommend'];//是否推荐
                    $id=$row['id'];   //商品ID
//                    echo "<tr><td>$name  $stock $onSale</td></tr>";
                ?>
                    <tr>
                    <td class="t1"><a href="/Admin/Goods/index/cid/49.html"><?php echo $categoryId; ?></a></td>
			<td><?php echo $name;?></td>
                        <td><?php echo $stock; ?></td>
			<td><a href="#" class="act-onsale" data-id="<?php echo $id;?>" data-status="yes"><?php echo $onSale; ?></a></td>
			<td><a href="#" class="act-recommend" data-id="<?php echo $id;?>" data-status="no"><?php echo $recommend; ?></a></td>
                        <td><a href="goodsedit.php?id=<?php echo $id;?>&pid=<?php echo $categoryId;?>">修改</a>　<a href="goodsdel.php?id=<?php echo $id; ?>" class="act-del" data-id="<?php echo $id;?>">删除</a></td>
                    </tr>
                <?php
                }
                ?>
        </table>
</div>
<div class="pagelist">
    <a class="num" href="goodslist.php?page=1">首页</a>
        <a class="num" href="goodslist.php?page=<?php echo $pre; ?>&cid=<?php echo $cid;?>">上一页</a>
        <a class="num" href="goodslist.php?page=<?php echo $next; ?>&cid=<?php echo $cid;?>">下一页</a>
        <a class="num" href="goodslist.php?page=<?php echo $maxPage; ?>&cid=<?php echo $cid;?>">尾页</a> 
</div>
<form method="post" id="form">
	<input type="hidden" name="id" id="target_id">
	<input type="hidden" name="field" id="target_field">
	<input type="hidden" name="status" id="target_status">
</form>
<script>
	//下拉菜单跳转
	$("#category").change(function(){
//		var url = "/Admin/Goods/index/cid/_ID_.html";
                var url="goodslist.php?cid=_ID_";
		location.href = url.replace("_ID_",$(this).val());
	});
	//快捷操作
	function change_status(obj,field){
		$("#target_id").val(obj.attr("data-id"));
		$("#target_field").attr("value",field)
		$("#target_status").attr("value",(obj.attr("data-status")=="yes") ? "no" : "yes");
		$("#form").attr("action","/Admin/Goods/change/p/0/cid/-1.html").submit();
	}
	//快捷操作-上架
	$(".act-onsale").click(function(){
		change_status($(this),'on_sale');
	});
	//快捷操作-推荐
	$(".act-recommend").click(function(){
		change_status($(this),'recommend');
	});
	//快捷操作-删除
	$(".act-del").click(function(){
		if(confirm('确定要删除吗？')){
			$("#target_id").val($(this).attr("data-id"));
			$("#form").attr("action","/Admin/Goods/del/p/0/cid/-1.html").submit();
		}
	});
</script></div>
	</div>
</div>
<script>
	$("#Goods_index").addClass("curr");
</script>
</body>
</html>