<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>传智商城 - 后台管理系统</title>
	<link rel="stylesheet" href="../Public/Admin/css/style.css"/>
	<script src="../Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php include_once 'top.php'; ?>
<div class="main">
	<?php include_once 'left.php';?>
	<div class="content">
		<div class="item"><div class="title">商品添加</div>
         <form action="goodsinsert.php" method="post" enctype="multipart/form-data">
<div class="top-button">
    
    <?php
        //1.连接数据库
        $link=mysqli_connect("localhost", "root", "");//创建数据库的连接
        mysqli_select_db($link, "itcast");//选择要使用数据库
        mysqli_set_charset($link, "utf8");//设置连接的字符编码格式
        //2.操作数据表 写SQL模板 
        //查询分类列表数据
        $sql2="SELECT * FROM shop_category;"; //mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
        $result2=  mysqli_query($link, $sql2);
        ?>
           
	选择商品分类：<select name="pid" id="category">
		 <?php
                while($row=  mysqli_fetch_assoc($result2)){
                    $cid=$row['id'];
                    $cname=$row['name'];
//                    echo "<option value='14' >图书</option>";
                ?>
                    <option value='<?php echo $cid;?>' ><?php echo $cname;?></option>
                <?php   
                }
               ?> 
                
                </select>
        <a href="goodslist.php" class="light">商品列表</a>
        <a href="goodsadd.php">添加分类</a>
</div>
<?php
    // 允许上传的图片后缀
   $allowedExts = array("gif", "jpeg", "jpg", "png");
?>
<div class="list auto">
    
	<table class="t2 t4">
		<tr><th>商品名称：</th><td><input type="text" name="name" class="big"></td></tr>
		<tr><th>商品编号：</th><td><input type="text" name="sn" ></td></tr>
		<tr><th>商品价格：</th><td><input type="text" name="price" class="small"></td></tr>
		<tr><th>商品库存：</th><td><input type="text" name="stock" class="small"></td></tr>
		<tr><th>是否上架：</th><td><select name="on_sale"><option value="yes" selected>是</option><option value="no">否</option></select></td></tr>
		<tr><th>首页推荐：</th><td><select name="recommend"><option value="yes">是</option><option value="no" selected>否</option></select></td></tr>
                <tr><th>上传图片：</th><td><input type="file" name="thumb" />图片类型：<?php echo implode(" ; ", $allowedExts); ?></td></tr>
	</table>
	<div class="editor">
		<link href="../Public/Admin/js/umeditor/themes/default/css/umeditor.min.css" rel="stylesheet">
<script src="../Public/Admin/js/umeditor/umeditor.config.js"></script>
<script src="../Public/Admin/js/umeditor/umeditor.min.js"></script>
<script>
	$(function(){
		//载入在线编辑器
		UM.getEditor("myEditor",{
		"imageUrl":"/Admin/Goods/uploadImage.html", //图片上传提交地址
		"imagePath":"../Public/Uploads/desc/"  //图片显示地址
		});
	});
</script>
		<script type="text/plain" id="myEditor" name="desc"><p>请在此处输入商品详情。</p></script>
	</div>
	<div class="btn">
		<input type="submit" value="添加商品">
	</div>
	</form>
</div>
<script>
	//下拉菜单跳转
	$("#category").change(function(){
		var url = "/Admin/Goods/add/cid/_ID_.html";
		location.href = url.replace("_ID_",$(this).val());
	});
</script></div>
	</div>
</div>
<script>
	$("#Goods_add").addClass("curr");
</script>
</body>
</html>