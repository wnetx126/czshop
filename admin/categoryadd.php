<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>传智商城 - 后台管理系统</title>
	<link rel="stylesheet" href="../Public/Admin/css/style.css"/>
	<script src="../Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php include_once 'top.php'; ?>
<div class="main">
        <?php include_once 'left.php'; ?>
	<div class="content">
		<div class="item"><div class="title">分类添加</div>
<div class="top-button">
	相关操作：<a href="/Admin/Category/index.html" class="light">分类列表</a>
</div>
<div class="list auto">
    <form action="categoryinsert.php" method="post">
	<table class="t2 t3">
		<tr><th>分类名称：</th><td><input type="text" name="name"></td></tr>
	</table>
	<div class="btn">
		<input type="submit" value="添加分类">
	</div>
	</form>
</div>
</div>
	</div>
</div>
<script>
	$("#Category_add").addClass("curr");
</script>
</body>
</html>