<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>传智商城 - 后台管理系统</title>
	<link rel="stylesheet" href="../Public/Admin/css/style.css"/>
	<script src="../Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php include_once 'top.php'; ?>
<div class="main">
        <?php include_once 'left.php'; ?>
	<div class="content">
		<div class="item"><div class="title">修改商品</div>
        <form action="goodsmodify.php" method="post" enctype="multipart/form-data">
<div class="top-button">
    <?php
        //一:接收前端浏览器地址栏【超链接】传递过来的参数，通过get获取参数
        $id=$_GET['id'];//商品ID
        $pid=$_GET['pid'];//商品分类ID
    ?>
     <?php
       
        //1.连接数据库
        $link=mysqli_connect("localhost", "root", "");//创建数据库的连接
        mysqli_select_db($link, "itcast");//选择要使用数据库
        mysqli_set_charset($link, "utf8");//设置连接的字符编码格式
        //2.操作数据表 写SQL模板 
        
        //查询分类列表数据
        $sql2="SELECT * FROM shop_category;"; //mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
        $result2=  mysqli_query($link, $sql2);
        ?>
           
	修改商品分类：<select name="pid" id="category">
		 <?php
                while($row=  mysqli_fetch_assoc($result2)){
                    $cid=$row['id'];
                    $cname=$row['name'];
//                    echo "<option value='14' >图书</option>";
                ?>
                    <option value='<?php echo $cid;?>' <?php if($cid==$pid) {echo 'selected';} ?> ><?php echo $cname;?></option>
                <?php   
                }
               ?> 
                
                </select>
        <a href="goodslist.php" class="light">返回商品列表</a>
</div>
   <?php  

            
            //二:把接收的参数插入到数据表中
            //2.操作数据表，写SQL模板
            $sql="SELECT * FROM shop_goods WHERE id=$id;";
            $result=  mysqli_query($link, $sql);//select查询返回结果资源；delete，update ,insert 查询返回true或false
            //3.处理结果数据
       //     $row=  mysqli_fetch_array($result);// 取出来结果即是关联数组也是个索引数组
            $row=  mysqli_fetch_assoc($result);//取出来结果即是关联数组
       //     $row=  mysqli_fetch_row($result);//取出来结果是个索引数组
            $name=$row['name'];   //名称
            $price=$row['price']; //价格
            $sn=$row['sn'];       //编号
            $thumb=$row['thumb']; //图片
            $stock=$row['stock']; //库存
            $on_sale=$row['on_sale'];//是否上架
            $recommend=$row['recommend'];//是否推荐
            $desc=$row['desc'];//商品详情内容
     ?>         
<div class="list auto">
	<input type="hidden" name="id" value="<?php echo $id; ?>">
	<table class="t2 t4">
            <tr><th>商品名称：</th><td><input type="text" name="name" value="<?php echo $name; ?>" class="big"></td></tr>
		<tr><th>商品编号：</th><td><input type="text" name="sn" value="<?php echo $sn; ?>" ></td></tr>
		<tr><th>商品价格：</th><td><input type="text" name="price" value="<?php echo $price; ?>" class="small"></td></tr>
		<tr><th>商品库存：</th><td><input type="text" name="stock" value="<?php echo $stock; ?>" class="small"></td></tr>
		<tr><th>是否上架：</th><td><select name="on_sale">
                        <option value="yes" <?php if($on_sale=='yes'){ echo 'selected'; } ?>>是</option>
			<option value="no" <?php if($on_sale=='no'){ echo 'selected'; } ?> >否</option>
		</select></td></tr>
		<tr><th>首页推荐：</th><td><select name="recommend">
			<option value="yes" <?php if($recommend=='yes'){ echo 'selected'; } ?>>是</option>
			<option value="no" <?php if($recommend=='no'){ echo 'selected'; } ?>>否</option>
		</select></td></tr>
                <?php
                    // 允许上传的图片后缀
                   $allowedExts = array("gif", "jpeg", "jpg", "png");
                ?>
		<tr><th>修改图片：</th><td><input type="file" name="thumb" />图片类型：<?php echo implode(" ; ", $allowedExts); ?></td></tr>
		<tr><th>当前图片：</th><td>
							<img src="../Public/Uploads/small/<?php echo $thumb; ?>">		</td></tr>
	</table>
	<div class="editor">
		<link href="../Public/Admin/js/umeditor/themes/default/css/umeditor.min.css" rel="stylesheet">
<script src="../Public/Admin/js/umeditor/umeditor.config.js"></script>
<script src="../Public/Admin/js/umeditor/umeditor.min.js"></script>
<script>
	$(function(){
		//载入在线编辑器
		UM.getEditor("myEditor",{
		"imageUrl":"/Admin/Goods/uploadImage.html", //图片上传提交地址
		"imagePath":"../Public/Uploads/desc/"  //图片显示地址
		});
	});
</script>
		<script type="text/plain" id="myEditor" name="desc"><?php echo $desc; ?></script>
	</div>
	<div class="btn">
		<input type="submit" value="保存修改">
	</div>
	</form>
</div>
<script>
	//下拉菜单跳转
//	$("#category").change(function(){
//		var url = "/Admin/Goods/edit/id/45/cid/_ID_/p/2.html";
//		location.href = url.replace("_ID_",$(this).val());
//	});
</script></div>
	</div>
</div>
<script>
	$("#Goods_edit").addClass("curr");
</script>
</body>
</html>