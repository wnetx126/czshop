<!--

-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
    <?php
        include_once 'islogin.php';
        include_once 'functions.php';
    ?>
    <?php
        /**
        * 功能：提交订单
        * 描述：在购物车中提交订单并跳转到订单列表页面
        *      订单表（订单编码，用户ID，商品总价）
        *      订单详情表（订单编码，用户ID，商品ID，商品数量）
        * 思路：1.生成订单表数据；
        *      2.生成订单详情表数据
        *      3.清空购物车中该用户已下单的商品数据
        *      4.跳转到订单列表页
        * 
        */
        //一:接收前端传递过来的参数 post提交过来的 表单域组件  
        //表单域组件[主要是input组件 ]
        echo "添加一个订单<br>";
         var_dump($_POST);
         $ids=$_POST['id'];//商品ID的数组
         $nums=$_POST['num'];//商品数量的数组
         $prices=$_POST['price'];//商品单价的数组
         $totalPrice=$_POST['totalPrice'];//商品的总价（所有商品的价格的和）
         $userId=$_COOKIE['czuid'];//用户的ID
         $orderCode=  generateNum();//生成订单号
         echo implode(",", $ids)."<br>";
         echo implode(",", $nums)."<br>";
         echo implode(",", $prices)."<br>";
         echo " $totalPrice $userId $orderCode<br>";
//         exit();
        //二:把接收的参数插入到数据表中
        //1.连接数据库
        $link=mysqli_connect("localhost", "root", "");//创建数据库的连接
        mysqli_select_db($link, "itcast");//选择要使用数据库
        mysqli_set_charset($link, "utf8");//设置连接的字符编码格式
        //2.操作数据表，写SQL模板
        //2A.把订单数据插入订单表中
        //INSERT INTO shop_order(user_id,order_code,goods,address,price,cancel,payment)VALUES(14,'202011250911101KW8HO','','南华工商学院实验楼316',168,'no','no');
        $sql="INSERT INTO shop_order(user_id,order_code,goods,address,price,cancel,payment)VALUES($userId,'$orderCode','','南华工商学院实验楼316',$totalPrice,'no','no');";
        echo "订单表 $sql<br>";
        $result=  mysqli_query($link, $sql); //mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
//        exit();
        //2B.把订单数据插入订单详情表中
        //INSERT INTO shop_order_detail(user_id,order_id,order_code,good_id,good_num,good_price)VALUES(14,0,'20201125091113NYN9HY',28,2,56.00);
        //INSERT INTO shop_order_detail(user_id,order_id,order_code,good_id,good_num,good_price)VALUES(14,0,'202011250911101KW8HO',29,1,56.00);
        $gcount=count($ids);
        for($i=0;$i<$gcount;$i++){
            $sqlDetail="INSERT INTO shop_order_detail(user_id,order_id,order_code,good_id,good_num,good_price)VALUES($userId,0,'$orderCode',$ids[$i],$nums[$i],$prices[$i]);";
            echo "订单详情表 $sqlDetail<br>";
            $result=  mysqli_query($link, $sqlDetail);//mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
        }
//      exit();
//      //2C.清空购物车中对应的商品ID的数据
//      DELETE FROM shop_cart WHERE user_id=14 AND goods_id=29;
        for($i=0;$i<$gcount;$i++){
            $sqlDel="DELETE FROM shop_cart WHERE user_id=$userId AND goods_id=$ids[$i];";
            $result= mysqli_query($link, $sqlDel);//mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
        }
        //3.处理结果记录
        if($result){
            echo "<script>alert('添加订单成功！');location.href='order.php'</script>";
        }else{
            echo "<script>alert('添加订单失败！');;location.href='order.php'</script>";
        }
    ?>
    </body>
</html>
