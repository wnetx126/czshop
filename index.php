<!DOCTYPE html>
<html>
<head>
	<title>传智商城首页</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="./Public/Home/css/style.css"/>
	<script src="./Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php
  include_once 'top.php';   //include 可以把其他的php页面包含进入当前的页面位置
?>
<div class="box">
        <?php include_once 'header.php'; ?>
	<?php include_once 'nav.php';?>
<!--分类左栏-->
<div class="slide">
<div class="cate">
	<div class="subcate left"><a href="/Index/find/cid/14.html">图书</a></div>
	<div class="subitem" style="display:none;">
		<dl><dt><a href="/Index/find/cid/15.html">音像</a></dt><dd><a href="/Index/find/cid/62.html">音乐</a><a href="/Index/find/cid/63.html">影视</a><a href="/Index/find/cid/64.html">游戏</a></dd></dl><dl><dt><a href="/Index/find/cid/16.html">IT类</a></dt><dd><a href="/Index/find/cid/17.html">PHP书籍</a><a href="/Index/find/cid/18.html">JAVA书籍</a><a href="/Index/find/cid/19.html">MySQL书籍</a><a href="/Index/find/cid/34.html">C语言书籍</a><a href="/Index/find/cid/49.html">网页书籍</a></dd></dl><dl><dt><a href="/Index/find/cid/35.html">少儿</a></dt><dd><a href="/Index/find/cid/36.html">少儿英语</a><a href="/Index/find/cid/37.html">少儿文学</a></dd></dl><dl><dt><a href="/Index/find/cid/38.html">管理</a></dt><dd><a href="/Index/find/cid/39.html">经济</a><a href="/Index/find/cid/40.html">金融</a><a href="/Index/find/cid/41.html">投资</a></dd></dl><dl><dt><a href="/Index/find/cid/42.html">生活</a></dt><dd><a href="/Index/find/cid/43.html">旅游</a><a href="/Index/find/cid/44.html">运动</a></dd></dl><dl><dt><a href="/Index/find/cid/45.html">艺术</a></dt><dd><a href="/Index/find/cid/46.html">摄影</a><a href="/Index/find/cid/47.html">设计</a><a href="/Index/find/cid/48.html">绘画</a></dd></dl>	</div>
</div><div class="cate">
	<div class="subcate left"><a href="/Index/find/cid/54.html">家具</a></div>
	<div class="subitem" style="display:none;">
			</div>
</div><div class="cate">
	<div class="subcate left"><a href="/Index/find/cid/55.html">手机</a></div>
	<div class="subitem" style="display:none;">
			</div>
</div><div class="cate">
	<div class="subcate left"><a href="/Index/find/cid/56.html">服装</a></div>
	<div class="subitem" style="display:none;">
			</div>
</div><div class="cate">
	<div class="subcate left"><a href="/Index/find/cid/57.html">家用电器</a></div>
	<div class="subitem" style="display:none;">
			</div>
</div><div class="cate">
	<div class="subcate left"><a href="/Index/find/cid/58.html">电脑、办公</a></div>
	<div class="subitem" style="display:none;">
			</div>
</div><div class="cate">
	<div class="subcate left"><a href="/Index/find/cid/59.html">运动户外</a></div>
	<div class="subitem" style="display:none;">
			</div>
</div><div class="cate">
	<div class="subcate left"><a href="/Index/find/cid/60.html">家具、厨具</a></div>
	<div class="subitem" style="display:none;">
			</div>
</div><div class="clear"></div>
</div>
<script>
	$(".cate").hover(function(){
		$(this).find(".subitem").show();
		$(this).children(".subcate").children("a").addClass("on");
	},function(){
		$(this).find(".subitem").hide();
		$(this).children(".subcate").children("a").removeClass("on");
	});
</script>
<!--热点图-->
<div class="hot left" id="hot">
	<div class="num"><a class="curr"></a><a></a><a></a><a></a></div>
	<ul>
		<li class="def"><a href="#"><img src="./Public/Uploads/hot/1.jpg" /></a></li>
		<li><a href="#"><img src="./Public/Uploads/hot/2.jpg" /></a></li>
		<li><a href="#"><img src="./Public/Uploads/hot/3.jpg" /></a></li>
		<li><a href="#"><img src="./Public/Uploads/hot/4.jpg" /></a></li>
	</ul>
</div>
<script src="./Public/Home/js/slideBox.js"></script>
<script>
	slideBox("#hot",5000);  //焦点图切换
</script>
<!--新闻列表-->
<div class="news right">最新动态</div>
<div class="clear"></div>
<!--推荐商品-->
<div class="best">
    
	<div class="best-title">精品推荐</div>
        <?php
        //1.连接数据库
        $link=  mysqli_connect("localhost", "root", "");//创建数据库的连接
        mysqli_select_db($link, "itcast");//选择你要操作的数据库
        mysqli_set_charset($link, "utf8");//设置数据库连接的字符编码
        //2.操作数据表，写SQL模板
        $sql="SELECT * FROM shop_goods WHERE recommend='yes'";
        $result=  mysqli_query($link, $sql); //mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
        //3.处理结果数据
        //$row=  mysqli_fetch_array($result);//返回一行数据包含索引数组和关联数组
        //$row= mysqli_fetch_assoc($result);//返回一行数据关联数组
        //$row=  mysqli_fetch_row($result);//返回一行数据索引数组

        
        while ($row= mysqli_fetch_assoc($result)) {
            $name=$row['name'];
            $price=$row['price'];
            $thumb=$row['thumb'];
            $id=$row['id'];
            //echo "$name  $price $thumb<br>";
        ?>
            <ul class="item left">
                <li><a href="goods.php?id=<?php echo $id; ?>" target="_blank"><img src="./Public/Uploads/small/<?php echo $thumb;?>"></a></li>
                <li class="goods"><a href="goods.php?id=<?php echo $id; ?>" target="_blank"><?php echo $name;  ?></a></li>
                <li class="price">￥<?php echo $price;?></li>
	    </ul>
        <?php
        }
        ?>


      
        
	
</div>
<script> $("#Index_index").addClass("curr"); </script>
	<div class="service">
		<ul><li>购物指南</li><li>配送方式</li><li>支付方式</li>
			<li>售后服务</li><li>特色服务</li><li>网络服务</li>
		</ul>
	</div>
	<div class="footer">传智商城·本项目仅供学习使用</div>
</div>
</body>
</html>