<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
     /**
     * 生成随机字符串实例代码（字母+数字）
     * 生成一个随机字符串时，总是先创建一个字符池，然后用一个循环和mt_rand()或rand()生成php随机数，从字符池中随机选取字符，最后拼凑出需要的长度
     * @param type $length
     * @return string
     */        
    function randomkeys($length) { 
     $pattern ="1234567890ABCDEFGHIJKLOMNOPQRSTUVWXYZ";
     $key="";
     for($i=0;$i<$length;$i++){ 
      $key .= $pattern{mt_rand(0,36)}; //生成php随机数 
     } 
     return $key; 
    } 
    echo "<br>";
    /**
     * 生成订单号
     * @return string
     */
    function generateNum(){

        $order_number =  date('Ymdhms').randomkeys(6);
        return $order_number;
    }
    echo generateNum();

    
    
?>
</html>
