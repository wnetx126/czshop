<!DOCTYPE html>
<!--
功能描述：添加商品到购物车
关键问题：如果某个用户的购物车已经有添加的商品，怎么处理？
解题思路：如果某个用户的购物车中没有已经添加的商品则直接添加到购物车中，如果某个用户的购物车中有已添加的商品则更新商品的数量
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php include_once 'islogin.php'; ?>
        <?php
        // 商品信息添加到购物车
        echo "添加购物车处理过程";
        $userId=$_COOKIE['czuid'];
        $num=$_POST['num'];//商品数量
        $goodsId=$_POST['goodsId'];//商品ID
        echo "$userId $num $goodsId<br>";
        //1.连接数据库      
        $link=  mysqli_connect("localhost", "root", "");    //创建数据库的连接
        mysqli_select_db($link, "itcast");//选择数据库
        mysqli_set_charset($link, "utf8");
        //2.操作数据表 写SQL模板
        //2.1查询当前用户的购物车中是否有已添加的商品信息
        $sql1="SELECT * FROM shop_cart WHERE user_id=$userId AND goods_id=$goodsId;";
        $result1=  mysqli_query($link, $sql1);
        $row1=  mysqli_fetch_assoc($result1);
        $result=false;//默认插入或更新失败
        if($row1){
          //如果查询购物车有已购商品，那么就更新商品数量  
          $sql2="UPDATE shop_cart SET num=num+$num WHERE user_id=$userId AND goods_id=$goodsId;";
          echo "<br>$sql2</br>";
          $result=mysqli_query($link, $sql2); //select 返回是结果资源；insert update delete 返回true或false
        }else{
          //查询购物车中没有已购商品，直接插入用户及商品信息  
          $sql3="INSERT INTO shop_cart(user_id,goods_id,num)VALUE($userId,$goodsId,$num);";
          echo "<br>$sql3</br>";
          $result=  mysqli_query($link, $sql3);//select 返回是结果资源；insert update delete 返回true或false
        }
        //exit();
        //3.处理结果数据
        if($result){
            echo "<script>alert('添加购物车成功！');location.href='cart.php'</script>";
        }else{
            echo "<script>alert('添加购物车失败！');;location.href='list.php'</script>";
        }
        ?>
    </body>
</html>
