<!DOCTYPE html>
<html>
<head>
	<title>购物车 - 传智商城</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="./Public/Home/css/style.css"/>
	<script src="./Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php include_once 'islogin.php'; ?>
<?php
  include_once 'top.php';   //include 可以把其他的php页面包含进入当前的页面位置
?>
<div class="box">
	<?php include_once 'header.php'; ?>
	<?php include_once 'nav.php';?>
<div class="usercenter">
<ul class="menu left">
	<li><a href="/User/index.html" id="User_index">个人信息</a></li>
	<li><a href="/Order/index.html" id="Order_index">我的订单</a></li>
	<li>我的关注</li>
	<li><a href="/User/addr.html" id="User_addr">收货地址</a></li>
	<li>消费记录</li>
	<li><a href="/Cart/index.html" id="Cart_index">购物车</a></li>
</ul>
<script>
$("#Cart_index").addClass("curr");
</script>
        <?php
        $userId=$_COOKIE['czuid'];
        //1.连接数据库
        $link=  mysqli_connect("localhost", "root", "");//创建数据库的连接
        mysqli_select_db($link, "itcast");//选择使用的数据库
        mysqli_set_charset($link, "utf8");//设置数据库编码连接方式
        //2.操作数据表，写SQL模板
        $sql="SELECT c.id as id ,c.user_id as user_id, g.id as goods_id ,g.name as `name`,g.price as price,c.num as num FROM shop_cart c,shop_goods g "
                . "WHERE c.goods_id=g.id AND c.user_id=$userId;";
        $result=  mysqli_query($link, $sql);//select 返回的结果资源；insert update delete 返回的true或false
        //3.处理结果记录
        ?>
	<div class="content left">我的购物车
            <form method="post" action="addorder.php">
		<table border="1" class="shopcart">
			<tr class="tr-tit"><th class="w50"><a href="javascript:void(0);" onclick="checkedAll()">全选</a></th><th>商品</th><th class="w50">单价</th><th class="w92">数量</th><th class="w50">操作</th></tr>
			<?php
                        while($row=  mysqli_fetch_assoc($result)){
                            $name=$row['name'];
                            $price=$row['price'];
                            $num=$row['num'];
                            $goodsId=$row['goods_id'];
                //            echo "<br>$name $price $num $goodsId<br>";
//                            echo "<h1>$name</h1>";
                        ?>
                        <tr class="item">
				<td class="center"><input type="checkbox" class="check" name="id[]" value="<?php echo $goodsId; ?>" /></td>
				<td><a href="goods.php?id=<?php echo $goodsId; ?>" target="_blank"><?php echo $name; ?></a></td>
				<td class="center">
                                    <span class="item-price"><?php echo $price; ?></span>
                                    <input type="hidden" name="price[]" value="<?php echo $price; ?>"/>
                                </td>
				<td><input class="setNum" type="button" value="-" /><input class="item-num" onkeyup="checkNum(this)" type="text" value="<?php echo $num; ?>" name="num[]"/><input class="setNum"  type="button" value="+" /></td>
				<td class="center"><a href="/Cart/del/id/8.html" />删除</a></td>
			</tr>
                        <?php
                        }
                        ?>
                      
                       	<tr><th><a href="javascript:void(0);" onclick="checkedAll()">全选</a></th><td colspan="4">删除选中的商品&nbsp;&nbsp;<a href="list.php">继续购物</a>&nbsp;&nbsp;
				共<span id="num"></span>件商品 总计：<span class="price">￥<span id="monery"></span></span><input type="hidden" id="totalPrice" name="totalPrice"/>
				<input type="submit" value="提交订单" class="order-btn" />
			</td></tr>
		</table>
		</form>
	</div>
	<div class="clear"></div>
</div>
<script>
	//点击修改数量
	$(".setNum").click(function () {
		if ($(this).val() === '-') {
			if ($(this).next().val() !== '1') {
				var num = $(this).next().val() - 1;
				$(this).next().attr("value", num);
				$(this).next().val(num);
			}
		}else if ($(this).val() === '+') {
			if ($(this).prev().val() !== '100') {
				var num = parseInt($(this).prev().val()) + parseInt(1);
				$(this).prev().attr("value", num);
				$(this).prev().val(num);
			}
		}
		func();
	});
	//键盘修改数量
	function checkNum(obj) {
		//判断当前值是否合法   凡是不合法的都重置为1
		var num = $(obj).val();
		if (num <= 1 || num >= 100) {
			$(obj).val(1);
		}
		func();
	}
	//默认情况下，设置为全选状态
	$(function () {
		$(":checkbox").prop("checked", true);
		$(":checkbox").attr("checked", true);
		func();
	});
	//全选
	function checkedAll() {
		$(":checkbox").each(function () {
			if (this.checked) {
				$(this).prop("checked", false);
				$(this).attr("checked", false);
			} else {
				$(this).prop("checked", true);
				$(this).attr("checked", true);
			}
		});
		func();
	}
	//单个选择时的状态设置
	$(".check").click(function () {
		$(this).each(function (i, v) {
			if (!v.checked) {
				$(this).prop("checked", false);
				$(this).attr("checked", false);
			} else {
				$(this).prop("checked", true);
				$(this).attr("checked", true);
			}
		});
		func();
	});
	//计算总价
	function func() {
		var price = 0;
		var num = 0;
		var totalnum = 0;
		$(".item").find(":checkbox:checked").each(function () {
			$(this).closest("tr").find(".item-num").each(function () {
				totalnum += parseInt($(this).val());
				num = parseInt($(this).val());
				$(this).closest("tr").find(".item-price").each(function () {
					price += parseInt($(this).text()) * num;
				});
			});
		});
		$("#monery").text(price);
		$("#num").text(totalnum);
		$("#totalPrice").attr("value",price);
	}
</script>

	<div class="service">
		<ul><li>购物指南</li><li>配送方式</li><li>支付方式</li>
			<li>售后服务</li><li>特色服务</li><li>网络服务</li>
		</ul>
	</div>
	<div class="footer">传智商城·本项目仅供学习使用</div>
</div>
</body>
</html>