<!DOCTYPE html>
<html>
<head>
	<title>PHP网站开发实例教程 - 传智商城</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="./Public/Home/css/style.css"/>
	<script src="./Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php
  include_once 'top.php';   //include 可以把其他的php页面包含进入当前的页面位置
?>
<div class="box">
	<?php include_once 'header.php'; ?>
	<?php include_once 'nav.php';?>
<div class="goodsinfo">
	<div class="now_cat">当前位置：&nbsp;<a 
		href="/Index/find/cid/14.html">图书</a>&nbsp;&gt;&nbsp;<a 
		href="/Index/find/cid/16.html">IT类</a>&nbsp;&gt;&nbsp;<a 
		href="/Index/find/cid/17.html">PHP书籍</a>&nbsp;&gt;&nbsp;PHP网站开发实例教程</div>
    <?php
//    var_dump($_GET);
    
    $id=$_GET['id'];
    
    ?>
    
    <?php
     //1.连接数据库
     $link=  mysqli_connect("localhost", "root", "");//连接数据库
     mysqli_select_db($link, "itcast");//选择要用数据库
     mysqli_set_charset($link, "utf8");//选择数据库连接的编码格式
     //2.操作数据表，写SQL模板
     $sql="SELECT * FROM shop_goods WHERE id=$id;";
     $result=  mysqli_query($link, $sql);//select查询返回结果资源；delete，update ,insert 查询返回true或false
     //3.处理结果数据
//     $row=  mysqli_fetch_array($result);// 取出来结果即是关联数组也是个索引数组
     $row=  mysqli_fetch_assoc($result);//取出来结果即是关联数组
//     $row=  mysqli_fetch_row($result);//取出来结果是个索引数组
     $name=$row['name'];   //名称
     $price=$row['price']; //价格
     $sn=$row['sn'];       //编号
     $thumb=$row['thumb']; //图片
     $stock=$row['stock']; //库存
     $id=$row['id'];//商品ID
     
    ?>
    <div class="pic left">		<img width="350" height="300" src="./Public/Uploads/small/<?php echo $thumb;  ?>" /></div>
	<div class="info left"><h1><?php echo $name;?></h1><table>
		<tr><th>售 价：</th><td><span class="price">￥<?php echo $price;?></span></td></tr>
		<tr><th>商品编号：</th><td><?php echo $sn;?></td></tr>
		<tr><th>累计销量：</th><td>1000</td></tr>
		<tr><th>评 价：</th><td>1000</td></tr>
		<tr><th>配送至：</th><td>北京（免运费）</td></tr>
		<tr><th>购买数量：</th><td>
                    <form method="post" action="addcart.php">
			<input type="button" value="-" class="cnt-btn" />
			<input type="text" value="1" id="num" name="num" class="num-btn" />
			<input type="button" value="+" class="cnt-btn" />（库存：<?php echo $stock;?>）</td></tr>
		<tr><td colspan="2" class="button">
                        <input type="hidden" name="goodsId" value="<?php echo $id; ?>">
                        <input type="submit" id="addCart" value="加入购物车" />
                    </form>
                        <form method="post" action="nowbuy.php" id="form_buy">
			<input type="hidden" name="gid" value="<?php echo $id; ?>">
                        <input type="hidden" name="price" value="<?php echo $price; ?>">
			<input type="hidden" name="num" value="" id="form_num">
		        <input type="hidden" name="__hash__" value="baf5a19ef959e3bb229e701abe5e2e4c_35d2c5e315602ee45ddfbdead1cec84c" />
                        <input type="submit" id="buy" value="立即购买"/>
                    </form>
                    </td></tr>
		</table></div>
		
		<div class="clear"></div>
	<div class="goods-slide left"><div class="title">相关商品推荐</div>
		<ul class="item left">
			<li><a href="/Index/goods/id/45.html" target="_blank"><img src="./Public/Uploads/small/2015-05/11/5550444272f89.png"></a></li>
			<li class="goods"><a href="/Index/goods/id/45.html" target="_blank">PHP网站开发实例教程</a></li>
			<li class="price">￥48.80</li>
		</ul><ul class="item left">
			<li><a href="/Index/goods/id/50.html" target="_blank"><img src="./Public/Uploads/small/2015-05/11/5550449f1d984.jpg"></a></li>
			<li class="goods"><a href="/Index/goods/id/50.html" target="_blank">PHP程序设计高级教程</a></li>
			<li class="price">￥45.00</li>
		</ul>	</div>
	<div class="desc left"><div class="title">商品详情</div><div class="content"><p class="nr" style="border:medium none;margin:0px;padding:0px;text-indent:28px;line-height:28px;color:rgb(88,88,88);font-family:arial, sans-serif;font-size:14px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;text-transform:none;white-space:normal;word-spacing:0px;"><span style="background-color:rgb(255,255,255);">PHP是一种运行于服务器端并完全跨平台的嵌入式脚本编程语言，是目前开发各类Web应用的主流语言之一。《PHP网站开发实例教程》就是面向初学者推出的一本案例驱动式教材，通过丰富实用的案例，全面讲解了PHP网站的开发技术。</span></p><p class="nr" style="border:medium none;margin:0px;padding:0px;text-indent:28px;line-height:28px;color:rgb(88,88,88);font-family:arial, sans-serif;font-size:14px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;text-transform:none;white-space:normal;word-spacing:0px;"><span style="background-color:rgb(255,255,255);">全书共9章，第1章讲解PHP开发环境搭建，通过部署网站的方式，让初学者了解基于PHP和MySQL的成熟开源项目的运行过程，第2章以趣味的案例学习PHP语法基础，第3章通过开发企业员工管理系统来学习PHP的数据库操作，第4通过用户登录注册、表单验证、保存用户资料、保存浏览历史、保存登录状态等案例学习Web表单与会话，第5章通过验证码、头像上传、缩略图、图片水印、文件管理器、在线网盘等案例来学习文件与图像技术，然后第6～8章通过常用类库封装、文章管理系统、学生管理系统等实用案例学习面向对象编程、PDO和ThinkPHP框架，最后一章通过开发实战项目——电子商城网站，综合运用本书所学的知识，让读者迅速积累项目开发经验。</span></p><p class="nr" style="border:medium none;margin:0px;padding:0px;text-indent:28px;line-height:28px;color:rgb(88,88,88);font-family:arial, sans-serif;font-size:14px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;text-transform:none;white-space:normal;word-spacing:0px;"><span style="background-color:rgb(255,255,255);">本书附有配套视频、源代码、习题、教学课件等资源，而且为了帮助初学者更好地学习本书讲解的内容，还提供了在线答疑，希望得到更多读者的关注。</span></p><p class="nr" style="border:medium none;margin:0px;padding:0px;text-indent:28px;line-height:28px;color:rgb(88,88,88);font-family:arial, sans-serif;font-size:14px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;text-transform:none;white-space:normal;word-spacing:0px;"><span style="background-color:rgb(255,255,255);">本书适合作为高等院校计算机相关专业程序设计或者Web项目开发的教材，也可作为PHP进阶提高的培训教材，是一本适合广大计算机编程爱好者的优秀读物。</span></p></div></div>
	<div class="clear"></div>
</div>
<script>
//导航条选中效果
$("#Index_find").addClass("category-curr");
//购买数量加减
$(".cnt-btn").click(function(){
	var num = parseInt($("#num").val());
	if ($(this).val() === '-') {
		if ( num=== 1) return;
		$("#num").val(num-1);
	}else if ($(this).val() === '+') {
		if (num === 107) return;
		$("#num").val(num+1);
	}
});
//自动纠正购买数量
$("#num").keyup(function(){
	var num = parseInt($(this).val());
	if(num<1){ 
		$(this).val(1);
	}else if(num > 107){
		$(this).val(107);
	}
});
//添加购物车
$("#addCart").click(function(){
	var url = "/Cart/add/id/45/num/_NUM_.html";
	window.location.href = url.replace("_NUM_",$("#num").val());
});
//购买
$("#buy").click(function(){
	$("#form_num").val($("#num").val());
	$("#form_buy").submit();
});
</script>
	<div class="service">
		<ul><li>购物指南</li><li>配送方式</li><li>支付方式</li>
			<li>售后服务</li><li>特色服务</li><li>网络服务</li>
		</ul>
	</div>
	<div class="footer">传智商城·本项目仅供学习使用</div>
</div>
</body>
</html>