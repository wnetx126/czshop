<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /**
         * 功能：删除一个订单
         * 描述：在订单列表中选择一个订单删除。
         * 思路：1.通过订单ID或订单号删除订单详情表数据
         *       2.通过订单ID或订单号删除订单表数据
         */
        //一:接收前端浏览器地址栏【超链接】传递过来的参数，通过get获取参数
        echo "删除一个订单";
        $orderCode=$_GET['code'];
        //二:把接收的参数插入到数据表中
        //1.连接数据库
        $link=mysqli_connect("localhost", "root", "");//创建数据库的连接
        mysqli_select_db($link, "itcast");//选择要使用数据库
        mysqli_set_charset($link, "utf8");//设置连接的字符编码格式
        //2.操作数据表，写SQL模板
        // DELETE FROM shop_order WHERE order_code='20201125091113NIOKCB';
        // DELETE FROM shop_order_detail WHERE order_code='20201125091113NIOKCB';
        //删除订单表数据 
        $sqlOrder="DELETE FROM shop_order WHERE order_code='$orderCode';";
        $result=  mysqli_query($link, $sqlOrder); //mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
        echo "<br>$sqlOrder</br>";
        //删除订单详情表数据
        $sqlOrderDetail="DELETE FROM shop_order_detail WHERE order_code='$orderCode';";
        $result=  mysqli_query($link, $sqlOrderDetail); //mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
        echo "<br>$sqlOrderDetail</br>";
//        exit();
        //3.处理结果记录
        if($result){
            echo "<script>alert('删除订单成功！');location.href='order.php'</script>";
        }else{
            echo "<script>alert('删除订单失败！');;location.href='order.php'</script>";
        }
        ?>
    </body>
</html>
