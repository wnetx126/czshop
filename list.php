<!DOCTYPE html>
<html>
<head>
	<title>商品列表 - 传智商城</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="./Public/Home/css/style.css"/>
	<script src="./Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php
  include_once 'top.php';   //include 可以把其他的php页面包含进入当前的页面位置
?>
<div class="box">
        <?php include_once 'header.php'; ?>
	<?php include_once 'nav.php';?>
<div class="find">
<div class="find-left left">
    <div class="title">相关商品推荐</div>
        <?php
        //1.连接数据库
        $link=mysqli_connect("localhost", "root", "");//创建数据库的连接
        mysqli_select_db($link, "itcast");//选择要使用数据库
        mysqli_set_charset($link, "utf8");//设置连接的字符编码格式
        //2.操作数据表 写SQL模板
        $sql1="SELECT * FROM shop_goods WHERE recommend='yes'";
        $result1=  mysqli_query($link, $sql1); //mysqli_query 返回结果：如果select返回结果资源,如果delete update insert返回的true或false
        //3.处理查询结果
        while ($row= mysqli_fetch_assoc($result1)) {
          $name=$row['name'];
          $price=$row['price'];
          $thumb=$row['thumb'];
          $id=$row['id'];
//          echo "$name  $price $thumb<br>";
        ?>
        <ul class="item left">
		<li><a href="/Index/goods/id/44.html" target="_blank"><img src="./Public/Uploads/small/<?php echo $thumb; ?>"></a></li>
		<li class="goods"><a href="/Index/goods/id/44.html" target="_blank"><?php echo $name; ?></a></li>
		<li class="price">￥<?php echo $price; ?></li>
	</ul>    
            
        <?php
        }
        ?>
 

        <ul class="item left">
		<li><a href="/Index/goods/id/45.html" target="_blank"><img src="./Public/Uploads/small/2015-05/11/5550444272f89.png"></a></li>
		<li class="goods"><a href="/Index/goods/id/45.html" target="_blank">PHP网站开发实例教程</a></li>
		<li class="price">￥48.80</li>
	</ul>
        <ul class="item left">
		<li><a href="/Index/goods/id/46.html" target="_blank"><img src="./Public/Uploads/small/2015-05/11/55502117ec92f.jpg"></a></li>
		<li class="goods"><a href="/Index/goods/id/46.html" target="_blank">MySQL数据库入门</a></li>
		<li class="price">￥39.50</li>
	</ul><ul class="item left">
		<li><a href="/Index/goods/id/47.html" target="_blank"><img src="./Public/Uploads/small/2015-05/11/5550216053882.jpg"></a></li>
		<li class="goods"><a href="/Index/goods/id/47.html" target="_blank">Java基础入门</a></li>
		<li class="price">￥44.50</li>
	</ul><ul class="item left">
		<li><a href="/Index/goods/id/48.html" target="_blank"><img src="./Public/Uploads/small/2015-05/11/555021e954397.jpg"></a></li>
		<li class="goods"><a href="/Index/goods/id/48.html" target="_blank">JavaWeb程序开发入门</a></li>
		<li class="price">￥44.50</li>
	</ul><ul class="item left">
		<li><a href="/Index/goods/id/50.html" target="_blank"><img src="./Public/Uploads/small/2015-05/11/5550449f1d984.jpg"></a></li>
		<li class="goods"><a href="/Index/goods/id/50.html" target="_blank">PHP程序设计高级教程</a></li>
		<li class="price">￥45.00</li>
	</ul>
</div>
 
<div class="find-right left">
        <?php
        //1.连接数据库
//        $link=mysqli_connect("localhost", "root", "");//创建数据库的连接
//        mysqli_select_db($link, "itcast");//选择要使用数据库
//        mysqli_set_charset($link, "utf8");//设置连接的字符编码格式
        //2.操作数据表 写SQL模板
         /**
        SELECT * FROM 表名称 LIMIT 起始值,长度;   起始值从0开始
        SELECT * FROM shop_goods LIMIT 0,6; 0  6*0= 6*(1-1)
        SELECT * FROM shop_goods LIMIT 6,6; 6  6*1= 6*(2-1)
        SELECT * FROM shop_goods LIMIT 12,6; 12 6*2= 6*(3-1)
        SELECT * FROM shop_goods LIMIT 18,6; 18 6*3= 6*(4-1)
        SELECT COUNT(id) AS num FROM shop_goods;
         */
        //查询商品总记录数
        $sqlNum="SELECT COUNT(id) AS num FROM shop_goods;";
        $resultNum=mysqli_query($link, $sqlNum);
        $rowNum=  mysqli_fetch_assoc($resultNum);
        $count=$rowNum['num'];//总记录数
        //page是通过超链接传递参数
        $page=  isset($_GET['page'])?$_GET['page']:1;
        $page=  intval($page);//页码数
        $psize=6;    //每一页商品数量
        $maxPage= ceil($count/$psize);  //尾页数即最大页数
        $page=$page<=$maxPage?$page:$maxPage;
        $start=$psize*($page-1);    //每一页起始值
        $pre=$page>1?$page-1:1;//上一页
        $next=$page<$maxPage?$page+1:$maxPage;//下一页
        //ceil(值) 取整到最接近的最大的整数 ceil(5.1)  结果是6  ceil(5.4)  结果是6  ceil(5.7)  结果是6 
        //查询商品列表数据
        //SELECT * FROM shop_goods LIMIT 0,6; 分页
//                $sql1="SELECT * FROM shop_goods WHERE category_id=$cid LIMIT $start,$psize;"; 
        $sql2="SELECT * FROM shop_goods  LIMIT $start,$psize;";
        $result2=  mysqli_query($link, $sql2);//select 返回的结果资源类型；insert delete update 返回结果是true或false
        //3.处理查询结果
        ?>
    
        
	<ul class="filter">
		<li class="filter-title">商品列表</li>
		<li><p>分类1：</p>
				<a href="/Index/find/cid/14.html" class="cid-14" >图书</a><a href="/Index/find/cid/54.html" class="cid-54" >家具</a><a href="/Index/find/cid/55.html" class="cid-55" >手机</a><a href="/Index/find/cid/56.html" class="cid-56" >服装</a><a href="/Index/find/cid/57.html" class="cid-57" >家用电器</a><a href="/Index/find/cid/58.html" class="cid-58" >电脑、办公</a><a href="/Index/find/cid/59.html" class="cid-59" >运动户外</a><a href="/Index/find/cid/60.html" class="cid-60" >家具、厨具</a></li><li><p>分类2：</p>
				<a href="/Index/find/cid/15.html" class="cid-15" >音像</a><a href="/Index/find/cid/16.html" class="cid-16" >IT类</a><a href="/Index/find/cid/35.html" class="cid-35" >少儿</a><a href="/Index/find/cid/38.html" class="cid-38" >管理</a><a href="/Index/find/cid/42.html" class="cid-42" >生活</a><a href="/Index/find/cid/45.html" class="cid-45" >艺术</a></li><li><p>分类3：</p>
				<a href="/Index/find/cid/17.html" class="cid-17" >PHP书籍</a><a href="/Index/find/cid/18.html" class="cid-18" >JAVA书籍</a><a href="/Index/find/cid/19.html" class="cid-19" >MySQL书籍</a><a href="/Index/find/cid/34.html" class="cid-34" >C语言书籍</a><a href="/Index/find/cid/49.html" class="cid-49" >网页书籍</a></li>		<li><p>价格：</p><a href="/Index/find/cid/16.html" class="price-0">全部</a>
			<a href="/Index/find/cid/16/price/0-10.html" class="price-0-10">0-10</a><a href="/Index/find/cid/16/price/11-20.html" class="price-11-20">11-20</a><a href="/Index/find/cid/16/price/21-30.html" class="price-21-30">21-30</a><a href="/Index/find/cid/16/price/31-40.html" class="price-31-40">31-40</a><a href="/Index/find/cid/16/price/41-50.html" class="price-41-50">41-50</a>		</li>
		<li><p>排序：</p><a
			href="/Index/find/cid/16.html" class="order-0">最新上架</a><a 
			href="/Index/find/cid/16/order/price-asc.html" class="order-price-asc">价格升序</a><a
			href="/Index/find/cid/16/order/price-desc.html" class="order-price-desc">价格降序</a>
		</li>
	</ul>
	<div class="find-item">
            <?php
            while ($row = mysqli_fetch_assoc($result2)) {
                $id=$row['id'];
                $name=$row['name'];   //名称
                $price=$row['price']; //价格
                $sn=$row['sn'];       //编号
                $thumb=$row['thumb']; //图片
                $stock=$row['stock']; //库存
//                echo "$name $price $sn<br>";
            ?>
             <ul class="item left">
			<li><a href="goods.php?id=<?php echo $id; ?>" target="_blank"><img src="./Public/Uploads/small/<?php echo $thumb; ?>"></a></li>
			<li class="goods"><a href="goods.php?id=<?php echo $id; ?>" target="_blank"><?php echo $name; ?></a></li>
			<li class="price">￥<?php echo $price; ?></li>
            </ul>
            <?php
            }
            ?>
           
            <div class="clear">
                
            </div>
		<div class="pagelist">
                    <?php
                    $showPages=3;//显示数字按钮数
                    $indexMax=($page+$showPages)<=$maxPage?($page+$showPages):$maxPage;  //按钮索引最大值
                    ?>
                    <a class="num" href="list.php?page=1">首页</a>
                    <a class="num" href="list.php?page=<?php echo $pre;?>"><<</a>  
                    <?php
                    for($index=$page;$index<=$indexMax;$index++){
                    ?>
                    <a class="num" href="list.php?page=<?php echo $index; ?>"><?php echo $index; ?></a>
                    <?php
                    }
                    ?>
                    <a class="num" href="list.php?page=<?php echo $next;?>">>></a>
                    <a class="num" href="list.php?page=<?php echo $maxPage; ?>">尾页</a>
                <div>  
            </div>
                    
            </div>
        </div>
</div>
</div>
<script>
//导航条选中效果
$("#Index_find").addClass("category-curr");
//筛选列表，分类的当前选中效果
$(".cid-16").addClass("curr");$(".cid-14").addClass("curr");$(".cid-0").addClass("curr");//商品价格的选中效果
	$(".price-0").addClass("curr");//商品排序的选中效果
	$(".order-0").addClass("curr");</script>
	<div class="service">
		<ul><li>购物指南</li><li>配送方式</li><li>支付方式</li>
			<li>售后服务</li><li>特色服务</li><li>网络服务</li>
		</ul>
	</div>
	<div class="footer">传智商城·本项目仅供学习使用</div>
</div>
</body>
</html>