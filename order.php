<!DOCTYPE html>
<html>
<head>
	<title>我的订单 - 传智商城</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="./Public/Home/css/style.css"/>
	<script src="./Public/Common/js/jquery.min.js"></script>
</head>
<body>
<?php include_once 'islogin.php'; ?>
<?php
  include_once 'top.php';   //include 可以把其他的php页面包含进入当前的页面位置
?>
<div class="box">
	<?php include_once 'header.php'; ?>
	<?php include_once 'nav.php';?>
<div class="usercenter">
	<?php include_once 'menuleft.php';?>
<script>
$("#Order_index").addClass("curr");
</script>
	<div class="content left">我的订单
                <?php
                $userId=$_COOKIE['czuid'];
                //1.连接数据库
                $link=  mysqli_connect("localhost", "root", "");//创建数据库的连接
                mysqli_select_db($link, "itcast");//选择使用的数据库
                mysqli_set_charset($link, "utf8");//设置数据库编码连接方式
                //2.操作数据表，写SQL模板
                //查询订单表得数据
                $sqlOrder="SELECT * FROM shop_order WHERE user_id=$userId;";
                $result=  mysqli_query($link, $sqlOrder);//select 返回的结果资源；insert update delete 返回的true或false
                //3.处理结果记录
                while ($row = mysqli_fetch_assoc($result)) {
                    $orderCode=$row['order_code'];
                    $address=$row['address'];
                    $totalPrice=$row['price'];
                    $orderTime=$row['add_time'];
                    $orderId=$row['id'];
                    
                ?>
                    <table class="order">
                        <tr class="tr-tit"><th class="txt-left" colspan="2">订单号：<?php echo $orderCode; ?>下单时间：<?php echo $orderTime; ?>　<a href="#">立即支付</a>　<a href="delOrder.php?code=<?php echo $orderCode ?>" data-id="<?php echo $orderId ?>" class="cancel">取消订单</a></th><th width="100">总价<span><?php echo $totalPrice; ?></span></th></tr>
                    <?php
                    //查询订单详情
                    $sqlDetail="SELECT g.id AS gid,d.good_num AS num ,g.name AS `name`,d.good_price as price FROM shop_order_detail d,shop_goods g WHERE d.good_id=g.id AND d.user_id=$userId AND d.order_code='$orderCode';";
    //                echo "$sqlDetail<br>";
                    $resultDetail=  mysqli_query($link, $sqlDetail);
                    while($row=  mysqli_fetch_assoc($resultDetail)){
                        $name=$row['name'];
                        $num=$row['num'];
                        $gid=$row['gid'];
                        $price=$row['price'];
                        $gid=$row['gid'];
                    ?>
                      <tr><td class="gname"><a href="goods.php?id=<?php echo $gid;?>" target="_blank"><?php echo $name;?></td>
                        <td class="txt-center"><?php echo $num;?>件</td>
                        <td class="txt-center"><span>￥<?php echo $price;?></span></td>
                      </tr>       
                    <?php
                    }
                    ?>
                    <tr><td colspan="3"><?php echo $address; ?></td></tr>
                    </table>    
                <?php
                }
                ?>
        </div>
	<form method="post" id="form">
		<input type="hidden" id="target" name="id">
	</form>
	<div class="clear"></div>
</div>
<script>
	$(".cancel").click(function(){
		if(confirm('您确定要取消订单？')){
			$("#target").val($(this).attr("data-id"));
			$("#form").attr("action","#").submit();
		}
	});
</script>
	<div class="service">
		<ul><li>购物指南</li><li>配送方式</li><li>支付方式</li>
			<li>售后服务</li><li>特色服务</li><li>网络服务</li>
		</ul>
	</div>
	<div class="footer">传智商城·本项目仅供学习使用</div>
</div>
</body>
</html>